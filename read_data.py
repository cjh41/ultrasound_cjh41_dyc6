def read_json(filename='bmode.json'):
    """Extracts data acquisition metadata from input JSON file.

    :param filename: name of the input JSON file (default is 'bmode.json').
    :return: fs: sampling frequency in Hz (int).
    :return: c: speed of sound in m/s (int).
    :return: axial_samples: number of samples in depth (int).
    :return: num_beams: number of lateral beams (int).
    :return: beam_spacing: spacing between lateral beams in m (float).
    """

    import json
    import logging
    import sys
    import errno

    # set up the log file
    logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    # open the file
    try:
        with open(filename, 'r') as f:
            bmode = json.load(f)
    except FileNotFoundError:
        logging.error('The JSON file could not be found.')
        sys.exit('The JSON file could not be found.')
    except (IOError, OSError) as ex:
        if ex.errno == errno.ENOSPC:
            logging.error('Ran out of space while trying to open JSON file.')
            sys.exit('Ran out of space while trying to open JSON file.')

    # check number of parameters in file
    try:
        if len(bmode) != 5:
            raise KeyError
    except KeyError:
        if len(bmode) < 5:
            logging.error('The JSON file does not contain enough parameters.')
            sys.exit('The JSON file does not contain enough parameters.')
        else:
            logging.debug('The JSON file has more than 5 parameters.')

    try:
        fs = bmode['fs']
        c = bmode['c']
        axial_samples = bmode['axial_samples']
        num_beams = bmode['num_beams']
        beam_spacing = bmode['beam_spacing']
    except KeyError:
        logging.error('Bad parameters in JSON file.')
        sys.exit('Bad parameters in JSON file.')

    logging.info('Read data acquisition metadata from JSON file.')

    return fs, c, axial_samples, num_beams, beam_spacing


def read_binary(filename='rfdat.bin'):
    """Extracts int16 binary data from a .bin file. Data should be ordered
    from shallow to deep for a single ultrasound beam, followed by subsequent
    lateral beams from left to right.

    :param filename: name of the input .bin file (default is 'rfdat.bin')
    :return: raw_data: raw ultrasound beam data (numpy array)
    """
    import numpy as np
    import logging
    import sys
    import errno

    logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    try:
        with open(filename, 'r'):
            raw_data = np.fromfile(filename, dtype='int16')
    except FileNotFoundError:
        logging.error('Binary data file could not be found.')
        sys.exit('Binary data file could not be found.')
    except (IOError, OSError) as ex:
        if ex.errno == errno.ENOSPC:
            logging.error('Ran out of space while trying to open binary file.')
            sys.exit('Ran out of space while trying to open binary file.')

    # check whether file was empty
    try:
        if len(raw_data) == 0:
            raise IOError
    except IOError:
        logging.error('Binary file did not contain any data.')
        sys.exit('Binary file did not contain any data.')

    logging.info('Read raw ultrasound data from binary data file.')
    return raw_data

if __name__ == '__main__':
    pass
