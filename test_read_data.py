def test_read_json():
    from read_data import read_json

    fs, c, ax_samp, num_beams, beam_spacing = read_json('bmode.json')

    assert fs == 40000000
    assert c == 1540
    assert ax_samp == 1556
    assert num_beams == 256
    assert beam_spacing == 0.00011746274509803921


def test_read_json_missing_file():
    from read_data import read_json
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.json'

    with pytest.raises(SystemExit) as cm:
        fs, c, ax_samp, num_beams, beam_spacing = read_json(temp_filepath)
    assert 'JSON file could not be found.' in str(cm.value)


def test_read_json_num_params():
    from read_data import read_json
    import json
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.json'

    # 0 parameters in JSON file
    test = {}
    with open(temp_filepath, 'w') as f:
        json.dump(test, f, indent=4)
    with pytest.raises(SystemExit) as cm:
        fs, c, ax_samp, num_beams, beam_spacing = read_json(temp_filepath)
    assert 'The JSON file does not contain enough parameters.' in str(cm.value)

    # less than 5 parameters in JSON file
    test = {'fs': 40000000, 'c': 1540, 'ax_samp': 1556}
    with open(temp_filepath, 'w') as f:
        json.dump(test, f, indent=4)
    with pytest.raises(SystemExit) as cm:
        fs, c, ax_samp, num_beams, beam_spacing = read_json(temp_filepath)
    assert 'The JSON file does not contain enough parameters.' in str(cm.value)

    # more than 5 parameters in JSON file, but the necessary ones are there
    test = {'fs': 40000000,
            'c': 1540,
            'axial_samples': 1556,
            'num_beams': 256,
            'beam_spacing': 0.00011746274509803921,
            'extra_param1': 123,
            'extra_param2': 0.7}
    with open(temp_filepath, 'w') as f:
        json.dump(test, f, indent=4)
    fs, c, ax_samp, num_beams, beam_spacing = read_json(temp_filepath)
    assert fs == 40000000
    assert c == 1540
    assert ax_samp == 1556
    assert num_beams == 256
    assert beam_spacing == 0.00011746274509803921


def test_read_json_bad_params():
    from read_data import read_json
    import json
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.json'

    # 0 parameters in JSON file
    test = {'fs': 40000000,
            'c': 1540,
            'axial_samples': 1556,
            'num_beams': 256,
            'not_beam_spacing': 0.00011746274509803921}
    with open(temp_filepath, 'w') as f:
        json.dump(test, f, indent=4)
    with pytest.raises(SystemExit) as cm:
        fs, c, ax_samp, num_beams, beam_spacing = read_json(temp_filepath)
    assert 'Bad parameters in JSON file.' in str(cm.value)


def test_read_binary():
    from read_data import read_binary

    raw_data = read_binary('rfdat.bin')
    test_length = len(raw_data)
    test_first_val = raw_data[0]
    test_last_val = raw_data[test_length - 1]
    test_middle_val = raw_data[7]

    assert test_length == 398336
    assert test_first_val == 0
    assert test_last_val == 2
    assert test_middle_val == -1


def test_read_binary_missing_file():
    from read_data import read_binary
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.bin'

    with pytest.raises(SystemExit) as cm:
        raw_data = read_binary(temp_filepath)
    assert 'Binary data file could not be found.' in str(cm.value)


def test_read_binary_empty_file():
    from read_data import read_binary
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.bin'

    with open(temp_filepath, 'wb') as g:
        g.write(b'')

    with pytest.raises(SystemExit) as cm:
        raw_data = read_binary(temp_filepath)
    assert 'Binary file did not contain any data.' in str(cm.value)
