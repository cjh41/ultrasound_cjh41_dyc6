.. Ultrasound documentation master file, created by
   sphinx-quickstart on Sun Nov 20 17:49:25 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ultrasound's documentation!
======================================

Contents:

.. toctree::
   :maxdepth: 2

   read_data
   process_data
   render_image
   ultrasound_main


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

