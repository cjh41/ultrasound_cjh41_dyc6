def test_save_image():
    from render_image import render_image
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import numpy as np
    import os.path

    lat = np.array([0, 1, 2, 3])
    ax = np.array([[0], [1], [2]])
    data = np.array([[1, 1, 1, 1], [2, 2, 2, 2], [3, 3, 3, 3]])
    filename = 'test_image.png'
    render_image(data, lat, ax, False, filename)
    if os.path.isfile(filename):
        file_exists = 1
    else:
        file_exists = 0
    assert file_exists == 1
