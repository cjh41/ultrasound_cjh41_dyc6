def create_axes(fs, c, ax_samp, num_beams, beam_spacing):
    """Creates axial and lateral meshgrid arrays for image display.

    :param fs: sampling frequency in Hz (int).
    :param c: speed of sound in m/s (int).
    :param ax_samp: number of samples in depth (int).
    :param num_beams: number of lateral beams (int).
    :param beam_spacing: spacing between lateral beams in m (float).
    :return: lat: 2D array of grid coordinates for lateral position (np.array).
    :return: ax: 2D array of grid coordiantes for axial position (np.array).
    """

    import numpy as np
    import logging

    # define lateral axis (left to right) assuming left-most point is 0
    lat_axis = np.arange(0, (num_beams+1)*beam_spacing, beam_spacing)
    # define axial axis (top to bottom) assuming top-most point is 0
    axial_axis = np.arange(0, 0.5*c*(ax_samp+1)/fs, 0.5*c/fs)

    lat, ax = np.meshgrid(lat_axis, axial_axis)

    logging.info('Created lateral and axial axes.')

    return lat, ax


def center_data(data):
    """Centers the input data about zero.

    :param data: input RF data from binary file (np.array).
    :return: centered_data: centered data (np.array).
    """

    import numpy as np
    import logging

    # calculate mean of the data and subtract from every value
    mean = np.mean(data)
    centered_data = data - mean

    logging.info('Centered the RF data about zero.')

    return centered_data


def reshape_array(centered_data, ax, lat):
    """Shapes data into a 2D array as prescribed by the axial and lateral
    dimensions

    :param centered_data: RF data, centered about zero (np.array)
    :param ax: number of axial samples (from JSON file) (int)
    :param lat: number of lateral samples (from JSON file) (int)
    :return: data_array: 2D array of accurate image size (np.array)
    """
    import numpy as np
    import logging
    import sys

    try:
        if len(centered_data) != ax*lat:
            raise KeyError
    except KeyError:
        if len(centered_data) != ax*lat:
            logging.error('JSON parameters do not match RF data.')
            sys.exit('JSON parameters do not match RF data.')

    data_array = np.zeros([ax, lat])
    env_counter = 0
    for j in range(lat):
        data_array[:, j] = centered_data[env_counter:env_counter + ax]
        env_counter += ax
    logging.info('Reshaped RF data into 2D array.')
    return data_array


def envelope_detect(data_array):
    """Returns the envelope of the mean-adjusted RF data array.

    :param data_array: 2D array of centered RF image data
    :returns: envelope: array of envelope of rectified RF data (np.array)
    """

    from scipy.signal import hilbert
    import numpy as np
    import logging

    transformed_data = hilbert(data_array)
    envelope = np.abs(transformed_data)
    logging.info('Calculated the envelope of the signal.')
    return envelope
    # re-write so it works on a column by column basis
    # modify unit test so that the data is replicated into a matrix


def log_compress(envelope):
    """Performs logarithmic compression on the envelope.

    :param envelope: envelope of the RF data from envelope_detect (np.array).
    :return: log_data: log-compressed data (np.array).
    """

    import numpy as np
    import logging

    # add 1 to every value before compressing to prevent negative values
    envelope_plus_one = envelope + 1

    log_data = np.log10(envelope_plus_one)

    logging.info('Performed logarithmic compression on envelope.')

    return log_data


if __name__ == '__main__':
    pass
