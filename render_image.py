def render_image(data_array, lat, ax, no_save=False,
                 filename='ultrasound_image.png'):
    """Renders B-mode image using matplotlib with axial and lateral dimensions
    labeled in meters.

    :param data_array: 2D array of image data
    :param lat: lateral distance array from create_axes()
    :param ax: axial distance array from create_axes()
    :param no_save: Boolean flag indicating if image is not saved, default no
    :param filename: name of .png image file if plot is saved
    """

    import matplotlib.pyplot as plt
    import logging
    import numpy as np
    import sys

    plt.ioff()
    plt.figure()
    plt.pcolormesh(lat, ax, data_array)
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel('Lateral distance (m)')
    plt.ylabel('Axial distance (m)')
    plt.xlim((0, np.max(lat)))
    plt.gray()
    plt.gca().invert_yaxis()
    if not no_save:
        try:
            plt.savefig(filename)
            logging.info('Saved image as PNG file')
        except (IOError, OSError) as ex:
            if ex.errno == errno.ENOSPC:
                logging.error(
                    'Ran out of space while trying to save PNG image.')
                sys.exit('Ran out of space while trying to save PNG image.')


def display_image():
    """Displays the current image using pcolormesh in matplotlib.
    """

    import matplotlib.pyplot as plt
    import logging

    plt.show()
    logging.info('Plot was displayed')
    plt.close()
