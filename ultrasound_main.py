import logging
from read_data import read_json, read_binary
from process_data import create_axes, center_data, reshape_array,\
    envelope_detect, log_compress
from render_image import render_image, display_image

# set up the log file
logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


def parse_cli():
    """ parse CLI

    :returns: args
    """

    import argparse as ap
    par = ap.ArgumentParser(description="Renders B-mode ultrasound image from"
                                        "binary RF data.",
                            formatter_class=ap.ArgumentDefaultsHelpFormatter)

    par.add_argument("-j", "--json", dest="json_filename",
                     help="name of JSON metadata file",
                     type=str, default='bmode.json')

    par.add_argument("-b", "--bin", dest="rf_filename",
                     help="name of RF binary data file",
                     type=str, default='rfdat.bin')

    par.add_argument("-d", "--display", dest="display",
                     help="display window containing rendered image",
                     action='store_true')

    par.add_argument("-n", "--no_save", dest="no_save",
                     help="image will not be saved as PNG file",
                     action='store_true')

    par.add_argument("-i", "--image", dest="image_filename",
                     help="filename of PNG image if saved",
                     type=str, default='ultrasound_image.png')

    args = par.parse_args()
    return args

if __name__ == '__main__':
    args = parse_cli()

    json_filename = args.json_filename
    rf_filename = args.rf_filename
    display = args.display
    no_save = args.no_save
    image_filename = args.image_filename

    fs, c, ax_samp, num_beams, beam_spacing = read_json(json_filename)
    binary_data = read_binary(rf_filename)

    lat_axis, ax_axis = create_axes(fs, c, ax_samp, num_beams, beam_spacing)
    centered_data = center_data(binary_data)
    reshaped_data = reshape_array(binary_data, ax_samp, num_beams)
    envelope = envelope_detect(reshaped_data)
    log_compressed_data = log_compress(envelope)

    render_image(log_compressed_data, lat_axis, ax_axis, no_save,
                 image_filename)
    if display:
        display_image()
