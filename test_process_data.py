def test_create_axes_dimensions():
    from process_data import create_axes

    lat, ax = create_axes(1, 2, 3, 4, 5)
    assert lat.shape == (4, 5)
    assert ax.shape == (4, 5)

    fs = 40000000
    c = 1540
    ax_samp = 1556
    num_beams = 256
    dx = 0.00011746274509803921

    lat, ax = create_axes(fs, c, ax_samp, num_beams, dx)

    assert lat.shape == (1557, 257)
    assert ax.shape == (1557, 257)


def test_create_axes_edges():
    from process_data import create_axes
    import numpy as np

    fs = 40000000
    c = 1540
    ax_samp = 1556
    num_beams = 256
    dx = 0.00011746274509803921

    lat, ax = create_axes(fs, c, ax_samp, num_beams, dx)

    assert np.array_equal(lat[0, :], np.arange(0, (num_beams+1)*dx, dx))
    assert np.array_equal(lat[ax_samp, :], np.arange(0, (num_beams+1)*dx, dx))
    assert np.array_equal(lat[:, 0], np.zeros(ax_samp+1))
    assert np.array_equal(lat[:, num_beams], np.full(ax_samp+1, num_beams*dx))

    assert np.array_equal(ax[0, :], np.zeros(num_beams+1))
    assert np.array_equal(ax[ax_samp, :], np.full(num_beams+1,
                                                  0.5*c*ax_samp/fs))
    assert np.array_equal(ax[:, 0], np.arange(0, 0.5*c*(ax_samp+1)/fs,
                                              0.5*c/fs))
    assert np.array_equal(ax[:, num_beams], np.arange(0, 0.5*c*(ax_samp+1)/fs,
                                                      0.5*c/fs))


def test_center_data():
    from process_data import center_data
    import numpy as np

    center_test = center_data(np.array([-2, 4, -3, 10, -15, 6]))
    assert np.array_equal(center_test, np.array([-2, 4, -3, 10, -15, 6]))

    center_test = center_data(np.array([8, 14, 7, 20, -5, 16]))
    assert np.array_equal(center_test, np.array([-2, 4, -3, 10, -15, 6]))

    center_test = center_data(np.array([11, 14, 7, 20, -5, 16]))
    assert np.array_equal(center_test, np.array([0.5, 3.5, -3.5, 9.5, -15.5,
                                                 5.5]))


def test_number_data_vals():
    from process_data import reshape_array
    from read_data import read_binary
    import numpy as np
    import pytest

    test_data = read_binary('rfdat.bin')
    ax = 1555  # 1556 is expected
    lat = 256
    with pytest.raises(SystemExit) as cm:
        data_array = reshape_array(test_data, ax, lat)
    assert 'JSON parameters do not match RF data.' in str(cm.value)


def test_reshape_array_values():
    from process_data import reshape_array
    from read_data import read_binary
    import numpy as np

    test_data = read_binary('rfdat.bin')
    test_array = reshape_array(test_data, 1556, 256)

    # check first and last columns
    known_first_col = test_data[0:1556]
    known_last_col = test_data[len(test_data)-1556:len(test_data)]
    array_first_col = test_array[:, 0]
    array_last_col = test_array[:, 255]
    assert np.array_equal(known_first_col, array_first_col)
    assert np.array_equal(known_last_col, array_last_col)


def test_reshaped_array_dim():
    from process_data import reshape_array
    from read_data import read_binary
    import numpy as np

    test_data = read_binary('rfdat.bin')
    ax = 1556
    lat = 256
    test_array = reshape_array(test_data, ax, lat)

    dim = np.shape(test_array)
    assert dim[0] == 1556
    assert dim[1] == 256


def test_envelope_detect():
    from process_data import envelope_detect, center_data, reshape_array
    import numpy as np

    # create sine wave of period 20, frequency 1/20 Hz.
    freq = 2*np.pi
    fs = 200
    ramp = np.arange(fs)  # x axis and ramp function
    y = [(np.sin(freq*(i/20))) for i in np.arange(fs)]

    # multiply sine wave by a ramp function
    ramp_sin = ramp * y

    rectified_signal = center_data(ramp_sin)

    # create an array
    test_array = reshape_array(rectified_signal, 10, 20)
    known_array = reshape_array(ramp, 10, 20)

    # envelope of this signal should be the ramp function
    # this will not be exact, so check that values fall within 10% tolerance
    test_envelope = envelope_detect(test_array)
    assert 0.90*known_array[4, 3] <= test_envelope[4, 3] <= \
        1.1*known_array[4, 3]
    assert 0.90*known_array[6, 18] <= test_envelope[6, 18] <= \
        1.1*known_array[6, 18]
    assert 0.90*known_array[7, 10] <= test_envelope[7, 10] <= \
        1.1*known_array[7, 10]


def test_log_compress():
    from process_data import log_compress
    import numpy as np

    log_test = log_compress(np.array([[9., 99., 999., 9999.],
                                      [99., 999., 999., 99999.],
                                      [99., 9., 99999., 9.]]))
    assert np.array_equal(log_test, np.array([[1., 2., 3., 4.],
                                              [2., 3., 3., 5.],
                                              [2., 1., 5., 1.]]))


def test_log_compress_lessthanone():
    from process_data import log_compress
    import numpy as np

    # test numbers between 0 and 1
    log_test = log_compress(np.array([[0.1, 0.2, 0.3],
                                      [0.9, 0.7, 0.5]]))
    actual_log = np.array([[np.log10(1.1), np.log10(1.2), np.log10(1.3)],
                           [np.log10(1.9), np.log10(1.7), np.log10(1.5)]])
    assert np.array_equal(log_test, actual_log)

    # test zero values
    log_test = log_compress(np.array([[0., 9., 0., 99., 0.],
                                      [999., 0., 99999., 9., 0.]]))
    assert np.array_equal(log_test, np.array([[0., 1., 0., 2., 0.],
                                              [3., 0., 5., 1., 0.]]))
