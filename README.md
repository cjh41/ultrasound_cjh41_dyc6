B-Mode Ultrasound Image Renderer
================================

This tool renders a B-mode ultrasound image from an input binary file containing RF data and an input JSON file containing the associated metadata. The user has the option to display the image, save the image, or both.

Instructions for Use
--------------------

* Clone this repository:
  + ``git clone git@gitlab.oit.duke.edu:cjh41/ultrasound_cjh41_dyc6.git``
* For help on how to use the program:
  + ``python ultrasound_main.py -h``
* The JSON metadata file must contain the following parameters:
  + `fs`: sampling frequency (Hz)
  + `c`: sound speed (m/s)
  + `axial_samples`: number of samples in depth
  + `num_beams`: number of lateral beams 
  + `beam_spacing`: spacing between lateral beams
* The input binary file must contain the RF data in int16 format.

License
-------
This software is licensed under the MIT license. See LICENSE.txt for more details.

Contributors
------------

* Collyn Heier (cjh41@duke.edu)
* Derek Chan (dyc6@duke.edu)